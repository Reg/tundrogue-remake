#ifndef _CONSTANTS_HPP_
#define _CONSTANTS_HPP_

// BearLibTerminal uses a 32-bit unsigned int for colors
// 0x AA RR GG BB
inline constexpr unsigned int G_COL_BLACK{ 0xFF000000u };
inline constexpr unsigned int G_COL_WHITE{ 0xFFFFFFFFu };
inline constexpr unsigned int G_COL_GRAY{ 0xFFB2B2B2u };
inline constexpr unsigned int G_COL_DARKEST_GRAY{ 0xFF121212u };
inline constexpr unsigned int G_COL_AMBER{ 0xFFFF9200u };
inline constexpr unsigned int G_COL_SEA{ 0xFF00FF7Fu };

// Tiles
inline constexpr int G_TILE_BLOCK{ L'█' };
inline constexpr int G_TILE_BOX_UL{ L'┏' };
inline constexpr int G_TILE_BOX_BL{ L'┗' };
inline constexpr int G_TILE_BOX_UR{ L'┓' };
inline constexpr int G_TILE_BOX_BR{ L'┛' };
inline constexpr int G_TILE_BOX_HOR{ L'━' };
inline constexpr int G_TILE_BOX_VER{ L'┃' };

// Layers
inline constexpr int G_LAYER_GAME{ 0 };
inline constexpr int G_LAYER_UI{ 2 };

// Width/Height of the game's window
constexpr inline int G_WINDOW_WIDTH{ 100 };
constexpr inline int G_WINDOW_HEIGHT{ 40 };

// Height of the bottom bar
constexpr inline int G_BAR_HOR{ 0 };
constexpr inline int G_BAR_VER{ 5 };

// Width/Height of the actual game
constexpr inline int G_WIDTH{ G_WINDOW_WIDTH - G_BAR_HOR };
constexpr inline int G_HEIGHT{ G_WINDOW_HEIGHT - G_BAR_VER };

#endif