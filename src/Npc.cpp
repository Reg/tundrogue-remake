#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#include "Npc.hpp"
#include "Inventory.hpp"
#include "Constants.hpp"
#include "utils.hpp"
#include "Game.hpp"
#include "Item.hpp"
#include <string.h>
#include <stdio.h>

Npc::Npc()
	: Npc(Position{}, 'D', 100, 5, 5, SPECIES::HUMAN, G_COL_WHITE, "NPC")
{
}

Npc::Npc(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name)
	: Entity(pos, character, health, attack, defense, species, color, name)
{
}

void Npc::Update()
{
	if(IsDead())
		return;

	if(utils::RandomBetween(0, 100) > 65)
	{
		DIRECTION direction{ utils::RandomBetween(1, (int) DIRECTION::MAX) };
		Move(direction);
	}
}

void Npc::Die()
{
	while(!m_Inventory->IsEmpty())
	{
		Item* item{ (*m_Inventory)[0] };
		m_Inventory->Drop(item, m_Position);
		/*item->SetPosition(m_Position);
		G_GAME->AddItem(item);
		m_Inventory->Remove(item);*/
	}
	const char* itemText{ "%s's corpse" };
	const size_t textLength{ strlen(itemText) };
	const size_t nameLength{ strlen(m_Name) };
	char nameBuffer[textLength + nameLength];
	sprintf(nameBuffer, itemText, m_Name);
	Item* corpse{ new Consumable{ m_Position, m_Char, 0, nameBuffer } };
	G_GAME->AddItem(corpse);
}
