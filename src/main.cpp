#include <iostream>
#include "BearLibWrapper.hpp"
#include "Game.hpp"
#include "Constants.hpp"
#include <random>

int main()
{
    // Open BearLibTerminal window
    if(!terminal_open())
    {
        std::cerr << "Couldn't open BearLibTerminal" << std::endl;
        return -1;
    }
    // seed random
    srand((unsigned int)time(nullptr));

    // Change window settings
    terminal_setf("window: title=TundRogue, size=%dx%d;input.filter=[keyboard, keypad, arrows];", G_WINDOW_WIDTH, G_WINDOW_HEIGHT);

    // Run game
    G_GAME = new Game{};
    G_GAME->Run();

    // Shutdown
    delete G_GAME;
    terminal_close();
    return 0;
}
