#ifndef _STRUCTS_HPP_
#define _STRUCTS_HPP_

// Shared structs go here

struct Position
{
	int x;
	int y;

	Position();
	Position(int x, int y);
	Position(float x, float y);
	friend bool operator==(const Position& lhs, const Position& rhs);
	friend Position operator-(const Position& lhs, const Position& rhs);
};
#endif