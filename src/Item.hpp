#ifndef _ITEM_HPP_
#define _ITEM_HPP_

#include "structs.hpp"

class Entity;

// Abstract Item class
class Item
{
public:
	explicit Item();
	explicit Item(Position position, int character, const char* name);
	Item(const Item& other) = delete;
	Item(Item&& other) noexcept = delete;
	Item& operator=(const Item& other) = delete;
	Item& operator=(Item&& other) noexcept = delete;
	virtual ~Item();

	virtual bool Use(Entity* user) = 0;
	void Draw() const;
	void SetPosition(const Position& pos);

	Position GetPosition() const;
	const char* GetName() const;
protected:
	Position m_Position;
	int m_Char;
	char* m_Name;
};


class Consumable : public Item
{
public:
	explicit Consumable();
	explicit Consumable(Position position, int character, int heal, const char* name);
	Consumable(const Consumable& other) = delete;
	Consumable(Consumable&& other) noexcept = delete;
	Consumable& operator=(const Consumable& other) = delete;
	Consumable& operator=(Consumable&& other) noexcept = delete;

	bool Use(Entity* user);
private:
	int m_Heal;
};
#endif
