// Remove VC++ warning for using strcpy
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#include "Game.hpp"
#include "BearLibWrapper.hpp"
#include "Entity.hpp"
#include "Npc.hpp"
#include "Player.hpp"
#include "Item.hpp"
#include "Map.hpp"
#include "utils.hpp"
#include "Constants.hpp"
#include <algorithm>
#include <string>
#include <cstdarg>

// Struct for message log
struct Message
{
	unsigned int color;
	char* text;

	Message(unsigned int color, const char* text)
		: color{ color }
		, text { nullptr }
	{
		utils::CopyString(&this->text, text);
	}

	~Message()
	{
		delete[] text;
	}
};

Game::Game() 
	: m_Running{ true }
	, m_CalculateFov{ false }
	, m_Player{}
	//, m_Player{ new Player{ Position{ G_WIDTH / 2, G_HEIGHT / 2 }, '@', 100, 5, 5, SPECIES::DRAGON, G_COL_SEA, "Player" } }
	, m_Entities{}
	, m_Map{ new Map{} }
{
	InitPlayer();
	m_Entities.push_back(m_Player);
	AddNPC(utils::RandomBetween(3, 10));
	AddItem(utils::RandomBetween(5, 7));
}

Game::~Game()
{
	for(int i{}; i < m_Entities.size(); ++i)
	{
		delete m_Entities[i];
	}
	m_Entities.clear();
	for(int i{}; i < m_Items.size(); ++i)
	{
		delete m_Items[i];
	}
	m_Items.clear();
	m_Player = nullptr;
	delete m_Map;
	m_Map = nullptr;
}

void Game::Run()
{
	// Calculate FOV and Draw for the first time
	m_Map->CalculateFov();
	Draw();
	while(m_Running)
	{
		m_PressedKey = utils::GetKey();

		if(m_PressedKey != -1)
		{
			Update();
			Draw();
		}
	}
}

void Game::Quit()
{
	m_Running = false;
}

void Game::AddMessage(unsigned int color, const char* text, ...)
{
	va_list args;
	char buffer[256];
	va_start(args, text);
	vsprintf(buffer, text, args);
	va_end(args);

	// -2 for log box
	if(m_Log.size() >= G_BAR_VER - 2)
	{
		Message* last{ m_Log[0] };
		std::vector<Message*>::iterator it{ std::find(m_Log.begin(), m_Log.end(), last) };
		delete last;
		m_Log.erase(it);
	}

	// Text array will get the right length by strlen in Message constructor
	m_Log.push_back(new Message{ color, buffer });
}

int Game::GetPressedKey() const
{
	return m_PressedKey;
}

Map* Game::GetMap() const
{
	return m_Map;
}

const Entity* Game::GetPlayer() const
{
	return m_Player;
}

const std::vector<Entity*>& Game::GetEntities()
{
	return m_Entities;
}

const std::vector<Item*>& Game::GetItems()
{
	return m_Items;
}

void Game::InitPlayer()
{
	int boxWidth{ 15 };
	int boxHeight{ 13 };
	const Position boxPos{ G_WINDOW_WIDTH / 2 - boxWidth / 2, G_WINDOW_HEIGHT / 2 };
	int choice{};
	int highlight{};
	SPECIES chosenSpecies{};
	bool chosen{ false };

	while(!chosen)
	{
		terminal_clear();
		utils::DrawBox(boxPos.x, boxPos.y, boxWidth, boxHeight, true, "CHOOSE", G_COL_WHITE, G_COL_BLACK);
		terminal_put(boxPos.x + 2, boxPos.y + 2 + highlight, '>');
		terminal_put(boxPos.x + 10, boxPos.y + 2 + highlight, '<');
		for(int i{}; i <= (int) SPECIES::MAX; ++i)
		{
			terminal_print(boxPos.x + 3, boxPos.y + 2 + i, Entity::GetSpeciesString((SPECIES) i));
		}
		terminal_refresh();

		choice = terminal_read();
		switch(choice)
		{
			case TK_UP:
			case TK_KP_8:
				--highlight;
				if(highlight == -1)
					highlight = (int) SPECIES::MAX;
				break;
			case TK_DOWN:
			case TK_KP_2:
				++highlight;
				if(highlight == (int) SPECIES::MAX + 1)
					highlight = 0;
				break;
			case TK_ENTER:
			case TK_KP_ENTER:
				chosenSpecies = SPECIES(highlight);
				chosen = true;
				break;
			default:
				break;
		}
	}

	m_Player = new Player{ Position{ G_WIDTH / 2, G_HEIGHT / 2 }, '@', 100, 15, 15, chosenSpecies, G_COL_SEA, "Player" };
}

int Game::GetKey() const
{
	if(terminal_has_input())
		return terminal_read();
	else
		return -1;
}

void Game::Draw() const
{
	terminal_clear();
	terminal_layer(G_LAYER_GAME);

	m_Map->Draw();

	for(auto& item : m_Items)
	{
		if(m_Map->IsVisible(item->GetPosition()))
			item->Draw();
	}

	// Reverse iteration to keep player always drawn on top (player will be drawn last)
	for(int i{ (int)m_Entities.size() - 1 }; i >= 0; --i)
	{
		if(m_Map->IsVisible(m_Entities[i]->GetPosition()))
			m_Entities[i]->Draw();
	}

	terminal_layer(G_LAYER_UI);
	DrawGui();

	terminal_refresh();
}

void Game::Update()
{
	m_Player->Update();

	if(m_CalculateFov)
		m_Map->CalculateFov();

	std::vector<Entity*> markedForDelete{};
	markedForDelete.reserve(m_Entities.size());
	// i = 1 to skip player
	for(int i{ 1 }; i < m_Entities.size(); ++i)
	{
		m_Entities[i]->Update();
		if(m_Entities[i]->IsDead())
		{
			markedForDelete.push_back(m_Entities[i]);
		}
	}

	for(auto& entity : markedForDelete)
	{
		std::vector<Entity*>::iterator it{ std::find(m_Entities.begin(), m_Entities.end(), entity) };
		delete entity;
		m_Entities.erase(it);
	}
}

void Game::AddNPC(int amount)
{
	for(int i{}; i < amount; ++i)
	{
		Position pos{};
		// Place the NPC on a non solid tile
		do
		{
			pos.x = utils::RandomBetween(0, m_Map->GetWidth() - 1);
			pos.y = utils::RandomBetween(0, m_Map->GetHeight() - 1);
		}
		while(m_Map->GetTile(pos).isSolid);

		SPECIES npcSpecies{ (SPECIES) utils::RandomBetween(0, (int) SPECIES::MAX) };
		int character{ Entity::GetSpeciesString(npcSpecies)[0] };
		char nameBuffer[128];
		sprintf(nameBuffer, "%s %d", Entity::GetSpeciesString(npcSpecies), i);
		Entity* npc
		{ 
			new Npc 
			{ 
				pos, 
				character, 
				utils::RandomBetween(7, 15), 
				utils::RandomBetween(1, 6), 
				utils::RandomBetween(1, 6), 
				npcSpecies,
				utils::RandomColor(), 
				nameBuffer 
			}
		};
		m_Entities.push_back(npc);
	}
}

void Game::AddItem(int amount)
{
	for(int i{}; i < amount; ++i)
	{
		Position pos{};
		// Place the item on a non solid tile
		do
		{
			pos.x = utils::RandomBetween(0, m_Map->GetWidth() - 1);
			pos.y = utils::RandomBetween(0, m_Map->GetHeight() - 1);
		} while(m_Map->GetTile(pos).isSolid);

		char nameBuffer[128];
		sprintf(nameBuffer, "Pizza %d", i);
		Item* item{ new Consumable{ pos, 'o', utils::RandomBetween(10, 25), nameBuffer} };
		m_Items.push_back(item);
	}
}

void Game::AddItem(Item* item)
{
	// Don't add nullptr
	if(item)
		m_Items.push_back(item);
}

void Game::RemoveItem(Item* item)
{
	std::vector<Item*>::iterator it{ std::find(m_Items.begin(), m_Items.end(), item) };
	m_Items.erase(it);
}

void Game::DrawGui() const
{
	if(G_BAR_HOR != 0)
	{
		for(int i{}; i < G_HEIGHT; ++i)
		{
			terminal_put(G_WIDTH, i, G_TILE_BOX_VER);
		}
	}

	// Print player stats
	const int statWidth{ 40 };
	int statX{};
	int statY{ G_HEIGHT };

	utils::DrawBox(statX, statY, statWidth, G_BAR_VER, true, "STATS", G_COL_WHITE, G_COL_BLACK);
	terminal_printf(++statX, ++statY, "Name: %s the %s", m_Player->GetName(), Entity::GetSpeciesString(m_Player->GetSpecies()));
	terminal_printf(statX, ++statY, "HP: %d", m_Player->GetHealth());
	terminal_printf(statX, ++statY, "ATT: %d  DEF: %d", m_Player->GetAttack(), m_Player->GetDefense());

	// Print log
	const int logWidth{ G_WINDOW_WIDTH - statWidth };
	const int logX{ G_WINDOW_WIDTH - logWidth };
	const int amountOfMessages{ (int)m_Log.size() };

	utils::DrawBox(logX, G_HEIGHT, logWidth, G_BAR_VER, true, "LOG", G_COL_WHITE, G_COL_BLACK);
	for(int i{}; i < amountOfMessages; ++i)
	{
		// +2 for bottom of log box
		int y{ i + 2 };
		terminal_color(m_Log[i]->color);
		terminal_print(logX + 1, G_WINDOW_HEIGHT - y, m_Log[i]->text);
	}
	terminal_color(G_COL_WHITE);
}
