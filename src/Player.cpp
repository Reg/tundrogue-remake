#include "Player.hpp"
#include "Inventory.hpp"
#include "Item.hpp"
#include "Constants.hpp"
#include "utils.hpp"
#include "Game.hpp"
#include "Map.hpp"
#include "BearLibWrapper.hpp"
#include <algorithm>

Player::Player()
	: Player(Position{}, '@', 100, 5, 5, SPECIES::DRAGON, G_COL_WHITE, "Player")
{
}

Player::Player(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name)
	: Entity(pos, character, health, attack, defense, species, color, name)
{
}

void Player::Update()
{
	if(IsDead())
		return;

	DIRECTION direction{};
	switch(G_GAME->GetPressedKey())
	{
		case TK_ESCAPE:
		// First GetKey() always return TK_CLOSE for some reason
		//case TK_CLOSE:
			G_GAME->Quit();
			break;
		// MOVEMENT
		case TK_KP_5:		// wait
			break;
		case TK_KP_1:		// lower left
			direction = DIRECTION::SOUTHWEST;
			break;
		case TK_DOWN:
		case TK_KP_2:
			direction = DIRECTION::SOUTH;
			break;
		case TK_KP_3:		// lower right
			direction = DIRECTION::SOUTHEAST;
			break;
		case TK_LEFT:
		case TK_KP_4:
			direction = DIRECTION::WEST;
			break;
		case TK_RIGHT:
		case TK_KP_6:
			direction = DIRECTION::EAST;
			break;
		case TK_KP_7:		// upper left
			direction = DIRECTION::NORTHWEST;
			break;
		case TK_UP:
		case TK_KP_8:
			direction = DIRECTION::NORTH;
			break;
		case TK_KP_9:		 // upper right
			direction = DIRECTION::NORTHEAST;
			break;
		// ACTIONS
		case TK_G:
		{
			if(terminal_state(TK_SHIFT)) // GIFT
			{
				InteractWithNpc(INTERACT_CHOICE::GIVE_ITEM);
			}
			else						 // GRAB
			{
				PickupItem();
			}
			break;
		}
		case TK_I:
			OpenInventory(INVENTORY_CHOICE::USE_ITEM);
			break;
		case TK_D:
			OpenInventory(INVENTORY_CHOICE::DROP_ITEM);
			break;
		case TK_F:
			if(terminal_state(TK_SHIFT)) // shift + f
			{
				InteractWithNpc(INTERACT_CHOICE::ATTACK);
			}
			break;
		#ifdef _DEBUG
		// -- Cheats --
		// Reveal map
		case TK_F1:
			for(int y{}; y < G_HEIGHT; ++y)
			{
				for(int x{}; x < G_WIDTH; ++x)
				{
					G_GAME->GetMap()->GetTile(x, y).explored = true;
				}
			}
			break;
		case TK_F2:
			G_GAME->AddMessage(utils::RandomColor(), "Test");
			break;
		case TK_F3:
			++G_FOV;
			G_GAME->AddMessage(G_COL_SEA, "FOV set to %d", G_FOV);
			G_GAME->m_CalculateFov = true;
			break;
		case TK_F4:
			--G_FOV;
			G_GAME->AddMessage(G_COL_SEA, "FOV set to %d", G_FOV);
			G_GAME->m_CalculateFov = true;
			break;
		case TK_F5:
		{
			int method{ (int) G_FOV_CALC + 1 };
			if(method > G_FOV_METHOD::CHEBYSHEV)
				G_FOV_CALC = G_FOV_METHOD::EUCLIDIAN;
			else
				G_FOV_CALC = (G_FOV_METHOD) method;


			G_GAME->AddMessage(G_COL_SEA, "FOV method set to %d", G_FOV_CALC);
			G_GAME->m_CalculateFov = true;
			break;
		}
		case TK_F6:
		{
			int method{ (int) G_FOV_CALC - 1 };
			if(method < G_FOV_METHOD::EUCLIDIAN)
				G_FOV_CALC = G_FOV_METHOD::CHEBYSHEV;
			else
				G_FOV_CALC = (G_FOV_METHOD) method;


			G_GAME->AddMessage(G_COL_SEA, "FOV method set to %d", G_FOV_CALC);
			G_GAME->m_CalculateFov = true;
			break;
		}
		#endif
	}

	// Recalculate FOV if player moved
	G_GAME->m_CalculateFov = Move(direction);
}

bool Player::Move(int x, int y)
{
	bool moved{ Entity::Move(x, y) };
	if(moved)
	{
		int amountOfItems{};
		Item* itemOnFloor{};
		for(auto& item : G_GAME->GetItems())
		{
			if(item->GetPosition() == m_Position)
			{
				itemOnFloor = item;
				++amountOfItems;
			}
		}

		if(amountOfItems == 1)
			G_GAME->AddMessage(G_COL_WHITE, "Here you see a %s", itemOnFloor->GetName());
		else if(amountOfItems > 1)
			G_GAME->AddMessage(G_COL_WHITE, "Here you see a bunch of items");

	}
	return moved;
}

bool Player::Move(DIRECTION direction)
{
	switch(direction)
	{
		case DIRECTION::NORTH:
			return Move(0, -1);
			break;
		case DIRECTION::SOUTH:
			return Move(0, 1);
			break;
		case DIRECTION::EAST:
			return Move(1, 0);
			break;
		case DIRECTION::WEST:
			return Move(-1, 0);
			break;
		case DIRECTION::NORTHEAST:
			return Move(1, -1);
			break;
		case DIRECTION::SOUTHEAST:
			return Move(1, 1);
			break;
		case DIRECTION::NORTHWEST:
			return Move(-1, -1);
			break;
		case DIRECTION::SOUTHWEST:
			return Move(-1, 1);
			break;
		default:
			return false;
			break;
	}
}

void Player::Die()
{
}

Item* Player::OpenInventory(INVENTORY_CHOICE choice)
{
	if(m_Inventory->IsEmpty())
	{
		G_GAME->AddMessage(G_COL_WHITE, "Your inventory is empty");
		return nullptr;
	}

	Position boxPos{ 1, 1 };
	int boxWidth{ 35 };
	int boxHeight{ int(2 + m_Inventory->GetSize()) };
	//const char* title{ "INVENTORY" };
	std::string title{ "INVENTORY - " };
	switch(choice)
	{
		case INVENTORY_CHOICE::USE_ITEM:
			title += "USE";
			break;
		case INVENTORY_CHOICE::DROP_ITEM:
			title += "DROP";
			break;
		case INVENTORY_CHOICE::PICK_ITEM:
			title += "CHOOSE";
	}

	utils::DrawBox(boxPos.x, boxPos.y, boxWidth, boxHeight, true, title.c_str(), G_COL_WHITE, G_COL_BLACK);
	for(int i{}; i < m_Inventory->GetSize(); ++i)
	{
		Item* item{ (*m_Inventory)[i] };
		int y{ boxPos.y + 1 + i };
		terminal_printf(boxPos.x + 1, y, "[color=yellow]%c[/color]. %s", char('a' + i), item->GetName());
	}
	terminal_refresh();

	int key{ utils::GetCharacter() };
	if(key != '\0')
	{
		unsigned int id{ (unsigned int)key - 'a' };
		Item* item{ (*m_Inventory)[id] };
		if(!item)
			return nullptr;

		switch(choice)
		{
			case INVENTORY_CHOICE::USE_ITEM:
				item->Use(this);
				m_Inventory->Remove(item);
				//G_GAME->AddMessage(G_COL_WHITE, "You use %s", item->GetName());
				delete item;
				break;
			case INVENTORY_CHOICE::DROP_ITEM:
				/*item->SetPosition(m_Position);
				G_GAME->AddItem(item);
				m_Inventory->Remove(item);*/
				m_Inventory->Drop(item, m_Position);
				G_GAME->AddMessage(G_COL_WHITE, "You drop %s", item->GetName());
				break;
			case INVENTORY_CHOICE::PICK_ITEM: // just return chosen item
			default:
				break;
		}
		return item;
	}
	return nullptr;
}

bool Player::PickupItem()
{
	std::vector<Item*> canBePicked{};
	for(auto& item : G_GAME->GetItems())
	{
		if(item->GetPosition() == m_Position)
			canBePicked.push_back(item);
	}

	if(canBePicked.empty())
		return false;
	else if(canBePicked.size() == 1)
	{
		if(m_Inventory->Add(canBePicked[0]))
		{
			G_GAME->RemoveItem(canBePicked[0]);
			G_GAME->AddMessage(G_COL_WHITE, "You pick up %s", canBePicked[0]->GetName());
			return true;
		}
		else
		{
			G_GAME->AddMessage(G_COL_WHITE, "You don't have enough space");
			return false;
		}
	}

	Position boxPos{ 1, 1 };
	int boxWidth{ 35 };
	int boxHeight{ int(2 + canBePicked.size()) };

	int itemCount{ 0 };
	std::vector<Item*> picked{};
	while(true)
	{
		int input{ -1 };
		int selected{ -1 };
		utils::DrawBox(boxPos.x, boxPos.y, boxWidth, boxHeight, true, "PICK UP", G_COL_WHITE, G_COL_BLACK);
		for(int i{}; i < canBePicked.size(); ++i)
		{
			Item* item{ canBePicked[i] };
			int y{ boxPos.y + 1 + i };
			bool selected{ std::find(picked.begin(), picked.end(), item) != picked.end() };
			if(selected)
				terminal_printf(boxPos.x + 1, y, "[color=yellow]%c[/color]. [[*]] %s", char('a' + i), item->GetName());
			else
				terminal_printf(boxPos.x + 1, y, "[color=yellow]%c[/color]. [[ ]] %s", char('a' + i), item->GetName());
		}
		terminal_refresh();

		input = utils::GetKey();

		// Pick up selected items
		if(input == TK_ENTER || input == TK_KP_ENTER)
		{
			if(m_Inventory->GetSlots() >= m_Inventory->GetSize() + itemCount)
				break;

			//G_GAME->AddMessage(G_COL_WHITE, "You don't have enough space");
			int width{ 28 };
			int height{ 3 };
			Position pos{ G_WINDOW_WIDTH / 2 - width, G_WINDOW_HEIGHT / 2 - height };
			utils::DrawBox(pos.x, pos.y, width, height, true, nullptr, G_COL_WHITE, G_COL_BLACK);
			terminal_printf(pos.x + 1, pos.y + 1, "You can't carry that much!");
			terminal_refresh();
			utils::GetKey();
			utils::ClearBox(pos.x, pos.y, width, height);
			terminal_refresh();
		}
		// Cancel
		else if(input == TK_ESCAPE)
			return false;

		// Select item
		selected = terminal_state(TK_WCHAR) - 'a';
		if(selected < canBePicked.size())
		{
			Item* item{ canBePicked[selected] };

			// Add or remove item
			std::vector<Item*>::iterator it{ std::find(picked.begin(), picked.end(), item) };
			if(it != picked.end())
			{
				picked.erase(it);
				--itemCount;
			}
			else
			{
				picked.push_back(item);
				++itemCount;
			}
		}
	}

	for(auto& item : picked)
	{
		m_Inventory->Add(item);
		G_GAME->RemoveItem(item);
	}

	return true;
}

bool Player::InteractWithNpc(INTERACT_CHOICE choice)
{
	std::vector<Entity*> near{ GetNearbyEntities() };
	if(near.empty())
	{
		G_GAME->AddMessage(G_COL_WHITE, "There's no one nearby");
		return false;
	}

	Position boxPos{ 1, 1 };
	int boxWidth{ 20 };
	int boxHeight{ 2 + (int)near.size() };
	const char* title{};

	switch(choice)
	{
		case INTERACT_CHOICE::ATTACK:
			title = "FIGHT";
			break;
		case INTERACT_CHOICE::GIVE_ITEM:
			title = "GIVE ITEM";
			break;
	}

	utils::DrawBox(boxPos.x, boxPos.y, boxWidth, boxHeight, true, title, G_COL_WHITE, G_COL_BLACK);
	for(int i{}; i < near.size(); ++i)
	{
		int y{ boxPos.y + 1 + i };
		Entity* entity{ near[i] };
		terminal_printf(boxPos.x + 1, y, "[color=yellow]%c[/color]. %s", char('a' + i), entity->GetName());
	}
	terminal_refresh();

	int key{ utils::GetCharacter() };
	if(key != '\0')
	{
		unsigned int id{ (unsigned int)key - 'a' };
		if(id >= near.size() || !near[id])
			return false;

		Entity* entity{ near[id] };
		switch(choice)
		{
			case INTERACT_CHOICE::ATTACK:
				Attack(entity);
				break;
			case INTERACT_CHOICE::GIVE_ITEM:
			{
				Item* gift{ OpenInventory(INVENTORY_CHOICE::PICK_ITEM) };
				if(!gift)
					return false;

				entity->GetInventory()->Add(gift);
				m_Inventory->Remove(gift);
				break;
			}
		}
		return true;
	}
	return false;
}
