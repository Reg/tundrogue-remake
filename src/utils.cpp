#include "utils.hpp"
#include "structs.hpp"
#include "BearLibWrapper.hpp"
#include "Constants.hpp"
#include <random>
#include <string.h>

// -- Colors
int utils::RandomBetween(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

unsigned int utils::RandomColor()
{
	uint8_t r{ uint8_t(utils::RandomBetween(64, UINT8_MAX)) };
	uint8_t b{ uint8_t(utils::RandomBetween(64, UINT8_MAX)) };
	uint8_t g{ uint8_t(utils::RandomBetween(64, UINT8_MAX)) };

	// Copied from BearLibTerminal.h:707
	return (UINT8_MAX << 24) | (r << 16) | (g << 8) | b;
}

// -- Distance
int utils::GetEuclidianDistance(const Position& start, const Position& end)
{
	Position diff{ end - start };
	return (int)sqrt(diff.x * diff.x + diff.y * diff.y);
}

// bad but works for FOV
int utils::GetSimpleEuclianDistance(const Position& start, const Position& end)
{
	Position diff{ end - start };
	return (diff.x * diff.x + diff.y * diff.y);
}

int utils::GetManhattanDistance(const Position& start, const Position& end)
{
	Position diff{ end - start };
	return std::abs(diff.x) + std::abs(diff.y);
}

int utils::GetChebyshevDistance(const Position& start, const Position& end)
{
	Position diff{ end - start };
	return std::max(std::abs(diff.x), std::abs(diff.y));
}

// -- Rendering
void utils::DrawBox(int x, int y, int width, int height, bool background, const char* title, unsigned int foregroundColor, unsigned int backgroundColor)
{
	// Fills background with blocks
	if(background)
	{
		terminal_layer(G_LAYER_UI);
		terminal_color(backgroundColor);

		for(int j{ y }; j < y + height; ++j)
		{
			for(int i{ x }; i < x + width; ++i)
			{
				terminal_put(i, j, G_TILE_BLOCK);
			}
		}
	}

	terminal_layer(G_LAYER_UI + (int)background);
	terminal_color(foregroundColor);

	for(int dx{ x }; dx < x + width; ++dx)
	{
		if(dx == x)
		{
			terminal_put(dx, y, G_TILE_BOX_UL);
			terminal_put(dx, y + height - 1, G_TILE_BOX_BL);
		}
		else if(dx == x + width - 1)
		{
			terminal_put(dx, y, G_TILE_BOX_UR);
			terminal_put(dx, y + height - 1, G_TILE_BOX_BR);
		}
		else
		{
			terminal_put(dx, y, G_TILE_BOX_HOR);
			terminal_put(dx, y + height - 1, G_TILE_BOX_HOR);
		}
	}

	for(int dy{ y + 1 }; dy < y + height - 1; ++dy)
	{
		terminal_put(x, dy, G_TILE_BOX_VER);
		terminal_put(x + width - 1, dy, G_TILE_BOX_VER);
	}

	// Just overwrite already written chars
	if(title)
		terminal_print(x + 1, y, title);

	//terminal_refresh();
	terminal_color(G_COL_WHITE);
}

void utils::ClearBox(int x, int y, int width, int height)
{
	terminal_layer(G_LAYER_UI + 1);
	terminal_clear_area(x, y, width, height);
	terminal_layer(G_LAYER_UI);
	terminal_clear_area(x, y, width, height);
}

int utils::GetKey()
{
	while(true)
	{
		int key{ terminal_read() };
		switch(key)
		{
			//case TK_RETURN:
			//case TK_ESCAPE:
			case TK_CLOSE:
				return L'\x0'; // cancel
				break;
			case TK_SHIFT:
			case TK_CONTROL:
			case TK_ALT:
			case TK_TAB:
				continue; // ignore
				break;
			default:
				return key;
				break;
		}
	}
}

int utils::GetCharacter()
{
	while(true)
	{
		switch(terminal_read())
		{
			case TK_RETURN:
			case TK_ESCAPE:
			case TK_CLOSE:
				return L'\x0'; // cancel
				break;
			case TK_SHIFT:
			case TK_CONTROL:
			case TK_ALT:
			case TK_TAB:
				continue; // ignore
				break;
			default:
				int key{ terminal_state(TK_WCHAR) };
				return key; // TK_(key) != terminal_state(TK_CHAR),
												 // this returns an actual character instead of BearLibTerminal code
				break;
		}
	}
}

void utils::CopyString(char** destination, const char* source)
{
	if(*destination)
		delete[] *destination;
	*destination = new char[strlen(source) + 1];
	strcpy(*destination, source);
	//return *destination;
}
