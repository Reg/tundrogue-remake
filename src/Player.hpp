#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "Entity.hpp"

class Item;

enum class INVENTORY_CHOICE
{
	USE_ITEM,
	DROP_ITEM,
	PICK_ITEM
};

enum class INTERACT_CHOICE
{
	ATTACK,
	GIVE_ITEM
};

class Player : public Entity
{
public:
	explicit Player();
	explicit Player(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name);
	Player(const Player& other) = delete;
	Player(Player&& other) noexcept = delete;
	Player& operator=(const Player& other) = delete;
	Player& operator=(Player&& other) noexcept = delete;

	void Update();
	bool Move(int x, int y);
	bool Move(DIRECTION direction);
	void Die();
private:
	Item* OpenInventory(INVENTORY_CHOICE choice);
	bool PickupItem();
	bool InteractWithNpc(INTERACT_CHOICE choice);
};

#endif