#ifndef _UTILS_HPP_
#define _UTILS_HPP_

// Header for various functions

struct Position;

namespace utils
{
	// Colors
	int RandomBetween(int min, int max);
	unsigned int RandomColor();

	// Distance
	int GetEuclidianDistance(const Position& start, const Position& end);
	int GetSimpleEuclianDistance(const Position& start, const Position& end);
	int GetManhattanDistance(const Position& start, const Position& end);
	int GetChebyshevDistance(const Position& start, const Position& end);

	// Rendering
	void DrawBox(int x, int y, int width, int height, bool background, const char* title, unsigned int foregroundColor, unsigned int backgroundColor);
	void ClearBox(int x, int y, int width, int height);

	// Input
	int GetKey();
	int GetCharacter();

	// c string stuff
	void CopyString(char** destination, const char* source);
}

#endif