#ifndef _NPC_HPP_
#define _NPC_HPP_

#include "Entity.hpp"

class Npc : public Entity
{
public:
	explicit Npc();
	explicit Npc(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name);
	Npc(const Npc& other) = delete;
	Npc(Npc&& other) noexcept = delete;
	Npc& operator=(const Npc& other) = delete;
	Npc& operator=(Npc&& other) noexcept = delete;

	void Update();
	void Die();
private:
};


#endif