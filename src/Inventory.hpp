#ifndef _INVENTORY_HPP_
#define _INVENTORY_HPP_

#include <vector>

struct Position;
class Item;

class Inventory
{
public:
	Inventory(int slots);
	Inventory(const Inventory& other) = delete;
	Inventory(Inventory&& other) noexcept = delete;
	Inventory& operator=(const Inventory& other) = delete;
	Inventory& operator=(Inventory&& other) noexcept = delete;
	~Inventory();

	bool Add(Item* item);
	Item* Remove(int index);
	Item* Remove(Item* item);
	Item* Drop(int index, const Position& position);
	Item* Drop(Item* item, const Position& position);
	bool IsEmpty() const;
	bool IsFull() const;
	int GetSize() const;
	int GetSlots() const;

	Item* operator[](size_t i);
private:
	int m_Slots;
	std::vector<Item*> m_Items;
};

#endif
