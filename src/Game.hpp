#ifndef _GAME_HPP_
#define _GAME_HPP_

#include <vector>

class Entity;
class Map;
class Item;

struct Message;

class Game final
{
public:
	explicit Game();
	// Don't need these constructors, just delete them
	Game(const Game& other) = delete;
	Game(Game&& other) noexcept = delete;
	Game& operator=(const Game& other) = delete;
	Game& operator=(Game&& other) noexcept = delete;
	~Game();

	void Run();
	void Quit();
	void AddMessage(unsigned int color, const char* text, ...);
	int GetPressedKey() const;
	Map* GetMap() const;
	const Entity* GetPlayer() const;
	const std::vector<Entity*>& GetEntities();
	const std::vector<Item*>& GetItems();

	void AddNPC(int amount);
	void AddItem(int amount);
	void AddItem(Item* item);
	void RemoveItem(Item* item);

	bool m_CalculateFov;
private:
	void InitPlayer();
	int GetKey() const;
	void Draw() const;
	void Update();

	void DrawGui() const;

	bool m_Running;
	int m_PressedKey;
	Entity* m_Player;
	std::vector<Entity*> m_Entities;
	std::vector<Item*> m_Items;
	std::vector<Message*> m_Log;
	Map* m_Map;
};

// This will be the only Game object
// With this other files can access G_GAME by just #including Game.hpp
inline Game* G_GAME;

inline int G_FOV{ 5 }; // Radius in tiles

enum G_FOV_METHOD
{
	EUCLIDIAN,
	SIMPLE,
	MANHATTAN,
	CHEBYSHEV
};
inline G_FOV_METHOD G_FOV_CALC{ G_FOV_METHOD::SIMPLE };
#endif