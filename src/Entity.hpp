#ifndef _ENTITY_HPP_
#define _ENTITY_HPP_

#include "structs.hpp"
#include <vector>

class Inventory;

enum class DIRECTION
{
	STAY,
	NORTH,
	SOUTH,
	EAST,
	WEST,
	NORTHEAST,
	SOUTHEAST,
	NORTHWEST,
	SOUTHWEST,
	MAX = SOUTHWEST
};

enum class SPECIES
{
	LIZARD,
	KOBOLD,
	DRAGON,
	WYVERN,
	HYDRA,
	EEL,
	SNAKE,
	HUMAN,
	MAX = HUMAN,
};

class Entity
{
public:
	explicit Entity();
	explicit Entity(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name);
	Entity(const Entity& other) = delete;
	Entity(Entity&& other) noexcept = delete;
	Entity& operator=(const Entity& other) = delete;
	Entity& operator=(Entity&& other) noexcept = delete;
	virtual ~Entity();

	// functions
	virtual void Update() = 0;
	void Draw() const;
	virtual bool Move(int x, int y);
	virtual bool Move(DIRECTION direction);
	void AddHealth(int heal);
	bool Attack(Entity* other);
	virtual void Die() = 0;

	std::vector<Entity*> GetNearbyEntities(int range = 1);
	bool IsDead() const;
	const char* GetName() const;
	SPECIES GetSpecies() const;
	static const char* GetSpeciesString(SPECIES species);
	int GetHealth() const;
	int GetAttack() const;
	int GetDefense() const;
	Position GetPosition() const;
	Inventory* GetInventory();
protected:
	// members
	Position m_Position;
	int m_Char;
	int m_Health;
	int m_Attack;
	int m_Defense;
	SPECIES m_Species;
	unsigned int m_Color; // BearLibTerminal uses unsigned int for color
	Inventory* m_Inventory;
	char* m_Name; // Names aren't going to be edited often, just use a char array instead of std::string
};

#endif
