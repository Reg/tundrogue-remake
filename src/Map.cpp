#include "Map.hpp"
#include "structs.hpp"
#include "BearLibWrapper.hpp"
#include "utils.hpp"
#include "Game.hpp"
#include "Entity.hpp"

Map::Map()
{
	m_Tiles = new MapTile[G_HEIGHT * G_WIDTH];
	GenerateMap();
}

Map::~Map()
{
	delete[] m_Tiles;
}

MapTile& Map::GetTile(int x, int y) const
{
	return m_Tiles[y * G_WIDTH + x];

}

MapTile& Map::GetTile(const Position& pos) const
{
	return GetTile(pos.x, pos.y);
}

bool Map::IsSolid(int x, int y) const
{
	return m_Tiles[y * G_WIDTH + x].isSolid;
}

bool Map::IsSolid(const Position& pos) const
{
	return IsSolid(pos.x, pos.y);
}

bool Map::IsVisible(int x, int y) const
{
	return m_Tiles[y * G_WIDTH + x].inFov;
}

bool Map::IsVisible(const Position& pos) const
{
	return IsVisible(pos.x, pos.y);
}

void Map::Draw() const
{
	for(int y{}; y < G_HEIGHT; ++y)
	{
		for(int x{}; x < G_WIDTH; ++x)
		{
			MapTile& currentTile{ m_Tiles[y * G_WIDTH + x] };

			if(currentTile.inFov)
			{
				currentTile.explored = true;
				terminal_color(G_COL_WHITE);
			}
			else if(currentTile.explored)
				terminal_color(G_COL_GRAY);
			else
				continue;

			switch(currentTile.character)
			{
				case '>':
				case '<':
					terminal_color(G_COL_AMBER);
					break;
				default:
					break;
			}

			terminal_put(x, y, currentTile.character);
		}
	}
}

void Map::GenerateMap()
{
	Position start{ G_WIDTH / 2, G_HEIGHT / 2 };
	Position currentPos{ start };
	int amountToGenerate{ utils::RandomBetween(G_HEIGHT * G_WIDTH / 6, G_HEIGHT * G_WIDTH / 4) };
	
	// Set first tile manually
	m_Tiles[start.y * G_WIDTH + start.x].character = '<';
	m_Tiles[start.y * G_WIDTH + start.x].isSolid = false;

	// Direction bias
	short horizontalDirection{ (short)utils::RandomBetween(0, 10) };
	short verticalDirection{ (short)utils::RandomBetween(0, 10) };
	/*
		-- HORIZONTAL --
		0 - RIGHT
		1 - LEFT

		-- VERTICAL --
		0 - UP
		1 - DOWN

		-- BOTH --
		>= 2 - CENTER
		
		Higher chance for CENTER direction
	*/

	#ifdef _DEBUG
		// Print direction when debugging
		const char* hDirString[] = { "RIGHT", "LEFT", "CENTER" };
		const char* vDirString[] = { "UP", "DOWN", "CENTER" };

		if(horizontalDirection > 2)
			std::printf("HORIZONTAL DIRECTION: %s\n", hDirString[2]);
		else
			std::printf("HORIZONTAL DIRECTION: %s\n", hDirString[horizontalDirection]);
		
		if(verticalDirection > 2)
			std::printf("VERTICAL DIRECTION: %s\n", vDirString[2]);
		else
			std::printf("VERTICAL DIRECTION: %s\n", vDirString[verticalDirection]);
	#endif

	int x;
	int y;
	do 
	{
		x = 0;
		y = 0;

		// x or y
		if(utils::RandomBetween(0, 1))
		{
			x = utils::RandomBetween(-1, 1); // can be 0

			// Bias
			if(x == 0)
			{
				switch(horizontalDirection)
				{
					case 0:
						++x;
						break;
					case 1:
						--x;
						break;
					case 2:
					default:
						if(utils::RandomBetween(0, 1))
							++x;
						else
							--x;
						break;
				}
			}

			// Reset position if out of bounds
			if(currentPos.x + x < 1 || currentPos.x + x >= G_WIDTH - 1)
			{
				currentPos = start;
			}
			currentPos.x += x;
		}
		else
		{
			y = utils::RandomBetween(-1, 1);

			if(y == 0)
			{
				switch(verticalDirection)
				{
					case 0:
						--y; // reversed y axis
						break;
					case 1:
						++y;
						break;
					case 2:
					default:
						if(utils::RandomBetween(0, 1))
							++y;
						else
							--y;
						break;
				}
			}

			if(currentPos.y + y < 1 || currentPos.y + y >= G_HEIGHT - 1)
			{
				currentPos = start;
			}
			currentPos.y += y;
		}

		int tileStyle;
		int index{ currentPos.y * G_WIDTH + currentPos.x };
		if(m_Tiles[index].isSolid)
		{
			tileStyle = utils::RandomBetween(0, 3);
			int tileChar;
			switch(tileStyle)
			{
				case 0:
					tileChar = ',';
					break;
				case 1:
					tileChar = '`';
					break;
				case 2:
					tileChar = '\'';
					break;
				case 3:
					tileChar = '.';
					break;
			}
			m_Tiles[index].isSolid = false;
			m_Tiles[index].character = tileChar;
			--amountToGenerate;
		}
	} 
	while(amountToGenerate != 0);

	Position exit{};
	do
	{
		exit.x = utils::RandomBetween(0, GetWidth() - 1);
		exit.y = utils::RandomBetween(0, GetHeight() - 1);
	} while(GetTile(exit).isSolid && GetTile(exit).character != '<');
	MapTile& tile = GetTile(exit);
	tile.character = '>';
}

int Map::GetWidth() const
{
	return G_WIDTH;
}

int Map::GetHeight() const
{
	return G_HEIGHT;
}

void Map::CalculateFov()
{
	Position playerPos{ G_GAME->GetPlayer()->GetPosition() };
	// Could be optimized?
	// Start from playerPos?
	for(int y{}; y < G_HEIGHT; ++y)
	{
		for(int x{}; x < G_WIDTH; ++x)
		{
			// TODO: check if there's a wall or something in the way
			int distance{};
			Position pos{ x, y };
			switch(G_FOV_CALC)
			{
				case G_FOV_METHOD::EUCLIDIAN:
					distance = utils::GetEuclidianDistance(pos, playerPos);
					break;
				case G_FOV_METHOD::SIMPLE:
					distance = utils::GetSimpleEuclianDistance(pos, playerPos);
					break;
				case G_FOV_METHOD::MANHATTAN:
					distance = utils::GetManhattanDistance(pos, playerPos);
					break;
				case G_FOV_METHOD::CHEBYSHEV:
					distance = utils::GetChebyshevDistance(pos, playerPos);
					break;
			}

			if(distance <= G_FOV)
				m_Tiles[y * G_WIDTH + x].inFov = true;
			else
				m_Tiles[y * G_WIDTH + x].inFov = false;
		}
	}
}
