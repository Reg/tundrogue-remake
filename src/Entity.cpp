#include "Entity.hpp"
#include "Inventory.hpp"
#include "Game.hpp"
#include "Map.hpp"
#include "BearLibWrapper.hpp"
#include "utils.hpp"

Entity::Entity()
	: Entity(Position{}, '@', 100, 5, 5, SPECIES::KOBOLD, G_COL_WHITE, "Unknown")
{
}

Entity::Entity(const Position& pos, int character, int health, int attack, int defense, SPECIES species, unsigned int color, const char* name)
	: m_Position{ pos }
	, m_Char{ character }
	, m_Health{ health }
	, m_Attack{ attack }
	, m_Defense{ defense }
	, m_Species{ species }
	, m_Color{ color }
	, m_Inventory{ new Inventory{ 10 } }
	, m_Name{ nullptr }
{
	utils::CopyString(&m_Name, name);
}

Entity::~Entity()
{
	delete m_Inventory;
	delete[] m_Name;
}

void Entity::Draw() const
{
	terminal_color(m_Color);
	terminal_put(m_Position.x, m_Position.y, m_Char);
	terminal_color(G_COL_WHITE);
}

bool Entity::Move(int x, int y)
{
	Position newPos{ m_Position.x + x, m_Position.y + y };
	
	// Check if tile is solid
	const MapTile& newTile{ G_GAME->GetMap()->GetTile(newPos) };
	if(newTile.isSolid)
		return false;

	// Check if there's another entity
	for(auto& entity : G_GAME->GetEntities())
	{
		if(entity->m_Position == newPos)
		{
			// Don't print message if current entity and entity on new position aren't visible
			if(G_GAME->GetMap()->IsVisible(m_Position) || newTile.inFov)
			{
				if(this == G_GAME->GetPlayer())
					G_GAME->AddMessage(G_COL_AMBER, "You bump into %s", entity->m_Name);
				else if(entity == G_GAME->GetPlayer())
					G_GAME->AddMessage(G_COL_AMBER, "%s bumps into you", m_Name);
				else
					G_GAME->AddMessage(G_COL_AMBER, "%s bumps into %s", m_Name, entity->m_Name);
			}
			// Stop entity
			//return false;
			// or swap positions
			entity->m_Position = m_Position;
			m_Position = newPos;
			return true;
		}
	}

	m_Position.x += x;
	m_Position.y += y;
	return true;
}

bool Entity::Move(DIRECTION direction)
{
	switch(direction)
	{
		case DIRECTION::NORTH:
			return Move(0, -1);
			break;
		case DIRECTION::SOUTH:
			return Move(0, 1);
			break;
		case DIRECTION::EAST:
			return Move(1, 0);
			break;
		case DIRECTION::WEST:
			return Move(-1, 0);
			break;
		case DIRECTION::NORTHEAST:
			return Move(1, -1);
			break;
		case DIRECTION::SOUTHEAST:
			return Move(1, 1);
			break;
		case DIRECTION::NORTHWEST:
			return Move(-1, -1);
			break;
		case DIRECTION::SOUTHWEST:
			return Move(-1, 1);
			break;
		default:
			return false;
			break;
	}
}

void Entity::AddHealth(int heal)
{
	m_Health += heal;
	if(IsDead())
		Die();
}

bool Entity::Attack(Entity* other)
{
	if(!other || other->IsDead())
		return false;

	int damage{ m_Attack * 3 / 2 };
	damage += utils::RandomBetween(-damage / 5 - 1, damage / 5 + 1);
	damage -= other->m_Defense * 2;
	if(damage < 0)
		damage = 1;

	other->AddHealth(-damage);
	if(other->IsDead())
	{
		if(this == G_GAME->GetPlayer())
			G_GAME->AddMessage(color_from_name("red"), "You kill %s! (DAM: %d)", other->m_Name, damage);
		else if(other == G_GAME->GetPlayer())
			G_GAME->AddMessage(color_from_name("red"), "%s kills you! (DAM: %d)", other->m_Name, damage);
		else
			G_GAME->AddMessage(color_from_name("red"), "%s kills %s! (DAM: %d)", m_Name, other->m_Name, damage);
	}
	else
	{
		if(this == G_GAME->GetPlayer())
			G_GAME->AddMessage(G_COL_AMBER, "You hit %s! (DAM: %d)", other->m_Name, damage);
		else if(other == G_GAME->GetPlayer())
			G_GAME->AddMessage(color_from_name("red"), "%s hits you! (DAM: %d)", other->m_Name, damage);
		else
			G_GAME->AddMessage(G_COL_WHITE, "%s hits %s! (DAM: %d)", m_Name, other->m_Name, damage);
	}
	return true;
}

std::vector<Entity*> Entity::GetNearbyEntities(int range)
{
	std::vector<Entity*> near{};
	auto& entities{ G_GAME->GetEntities() };
	for(int i{}; i < entities.size(); ++i)
	{
		if((entities[i] != this) && utils::GetChebyshevDistance(m_Position, entities[i]->m_Position) <= range)
			near.push_back(entities[i]);
	}
	return near;
}

bool Entity::IsDead() const
{
	return m_Health <= 0;
}

Position Entity::GetPosition() const
{
	return m_Position;
}

Inventory* Entity::GetInventory()
{
	return m_Inventory;
}

const char* Entity::GetName() const
{
	return m_Name;
}

SPECIES Entity::GetSpecies() const
{
	return m_Species;
}

const char* Entity::GetSpeciesString(SPECIES species)
{
	switch(species)
	{
		case SPECIES::LIZARD:
			return "Lizard";
			break;
		case SPECIES::KOBOLD:
			return "Kobold";
			break;
		case SPECIES::HUMAN:
			return "Human";
			break;
		case SPECIES::DRAGON:
			return "Dragon";
			break;
		case SPECIES::WYVERN:
			return "Wyvern";
			break;
		case SPECIES::HYDRA:
			return "Hydra";
			break;
		case SPECIES::EEL:
			return "Eel";
			break;
		case SPECIES::SNAKE:
			return "Snake";
			break;
		default: // should be impossible
			return "Unknown";
			break;
	}
}

int Entity::GetHealth() const
{
	return m_Health;
}

int Entity::GetAttack() const
{
	return m_Attack;
}

int Entity::GetDefense() const
{
	return m_Defense;
}
