#include "Item.hpp"
#include "Entity.hpp"
#include "BearLibWrapper.hpp"
#include "Constants.hpp"
#include "utils.hpp"
#include "Game.hpp"

// -- Base
Item::Item()
	: Item(Position{}, '?', "Unknown")
{
}

Item::Item(Position position, int character, const char* name)
	: m_Position{ position }
	, m_Char{ character }
	, m_Name{ nullptr }
{
	utils::CopyString(&m_Name, name);
}

Item::~Item()
{
	delete[] m_Name;
}

void Item::Draw() const
{
	terminal_color(G_COL_AMBER);
	terminal_put(m_Position.x, m_Position.y, m_Char);
	terminal_color(G_COL_WHITE);
}

void Item::SetPosition(const Position& pos)
{
	m_Position = pos;
}

Position Item::GetPosition() const
{
	return m_Position;
}

const char* Item::GetName() const
{
	return m_Name;
}

// -- Consumables
Consumable::Consumable()
	: Consumable(Position{}, '?', 0, "Consumable")
{
}

Consumable::Consumable(Position position, int character, int heal, const char* name)
	: Item(position, character, name)
	, m_Heal{ heal }
{
}

bool Consumable::Use(Entity* user)
{
	user->AddHealth(m_Heal);
	if(user == G_GAME->GetPlayer())
		G_GAME->AddMessage(G_COL_WHITE, "You use %s", m_Name);
	else
		G_GAME->AddMessage(G_COL_WHITE, "%s uses %s", user->GetName(), m_Name);

	return true;
}
