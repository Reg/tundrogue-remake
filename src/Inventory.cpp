#include "Inventory.hpp"
#include "Item.hpp"
#include "Game.hpp"
#include "structs.hpp"
#include <algorithm>

Inventory::Inventory(int slots)
	: m_Slots(slots)
{
	m_Items.reserve(slots);
}

Inventory::~Inventory()
{
	for(auto& item : m_Items)
	{
		delete item;
	}
	m_Items.clear();
}

bool Inventory::Add(Item* item)
{
	if(m_Items.size() >= m_Slots)
		return false;

	m_Items.push_back(item);
	return true;
}

Item* Inventory::Remove(int index)
{
	if(index >= m_Items.size())
		return nullptr;
	
	Item* item{ m_Items[index] };
	std::vector<Item*>::iterator it{ std::find(m_Items.begin(), m_Items.end(), item) };
	m_Items.erase(it);
	return item;
}

Item* Inventory::Remove(Item* item)
{
	if(!item)
		return nullptr;

	std::vector<Item*>::iterator it{ std::find(m_Items.begin(), m_Items.end(), item) };
	m_Items.erase(it);
	return item;
}

Item* Inventory::Drop(int index, const Position& position)
{
	if(index >= m_Items.size())
		return nullptr;

	Item* item{ m_Items[index] };
	std::vector<Item*>::iterator it{ std::find(m_Items.begin(), m_Items.end(), item) };
	m_Items.erase(it);
	item->SetPosition(position);
	G_GAME->AddItem(item);
	return item;
}

Item* Inventory::Drop(Item* item, const Position& position)
{
	if(!item)
		return nullptr;

	std::vector<Item*>::iterator it{ std::find(m_Items.begin(), m_Items.end(), item) };
	m_Items.erase(it);
	item->SetPosition(position);
	G_GAME->AddItem(item);
	return item;
}

bool Inventory::IsEmpty() const
{
	return m_Items.empty();
}

bool Inventory::IsFull() const
{
	return m_Items.size() >= m_Slots;
}

int Inventory::GetSize() const
{
	return m_Items.size();
}

int Inventory::GetSlots() const
{
	return m_Slots;
}

Item* Inventory::operator[](size_t i)
{
	if(i >= m_Items.size())
		return nullptr;
	else
		return m_Items[i];
}
