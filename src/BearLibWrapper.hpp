/* Wrapper for BearLibTerminal.h to disable VC++ warnings */

#ifndef BEARLIBTERMINAL_H 
		#ifdef _MSC_VER
		#define DISABLE_BAD_WARN __pragma(warning( disable : 4244 4626 5027 5031 4514 4996 6308 6386 28182 6385))
		#define _CRT_SECURE_NO_WARNINGS 1
		#define _CRT_NONSTDC_NO_WARNINGS 1
		#pragma warning(push)
		DISABLE_BAD_WARN
		#include <BearLibTerminal.h>
		#pragma comment(lib, "BearLibTerminal.lib")
		#pragma warning(pop)
	#else
		#include <BearLibTerminal.h>
	#endif
#endif