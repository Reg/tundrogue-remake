#ifndef _MAP_HPP_
#define _MAP_HPP_

#include "Constants.hpp"

struct Position;

struct MapTile
{
	bool isSolid{ true };
	bool explored{ false };
	bool inFov{ false };
	int character{ G_TILE_BLOCK };
};

class Map final 
{
public:
	explicit Map();
	~Map();

	MapTile& GetTile(int x, int y) const;
	MapTile& GetTile(const Position& pos) const;
	bool IsSolid(int x, int y) const;
	bool IsSolid(const Position& pos) const;
	bool IsVisible(int x, int y) const;
	bool IsVisible(const Position& pos) const;
	void Draw() const;
	int GetWidth() const;
	int GetHeight() const;
	void CalculateFov();
private:
	void GenerateMap();

	MapTile* m_Tiles;
};

#endif