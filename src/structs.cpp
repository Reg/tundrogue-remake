#include "structs.hpp"
#include <cmath>
// Position
Position::Position()
	: x{}
	, y{}
{
}

Position::Position(int x, int y)
	: x{ x }
	, y{ y }
{
}

Position::Position(float x, float y)
	: x{ (int)std::round(x) }
	, y{ (int)std::round(y) }
{
}

bool operator==(const Position& lhs, const Position& rhs)
{
	return (lhs.x == rhs.x && lhs.y == rhs.y);
}

Position operator-(const Position& lhs, const Position& rhs)
{
	return Position{ rhs.x - lhs.x, rhs.y - lhs.y };
}
